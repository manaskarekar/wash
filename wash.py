# Wash - A simple shell in python

import os, sys, subprocess

def tokenize(cmd):
	tokens = filter(None, cmd.split())
	return tokens

def shell_launch(tokens):
	pid = os.fork()

	if pid == 0:
		#print "Child %d" % os.getpid()
		#Run your command.
		print tokens
		os.execvp(tokens[0], tokens[1:])
	elif pid > 0:
		#print "Parent %d" % os.getpid()
		os.waitpid(pid, 0)
	else:
		print "Error running command."

def shell_loop():
	while(True):
		print ">",
		cmd = raw_input()

		print id(cmd)
		shell_launch(tokenize(cmd))

if __name__=="__main__":
	shell_loop()
